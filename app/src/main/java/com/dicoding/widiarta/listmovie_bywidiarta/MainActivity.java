package com.dicoding.widiarta.listmovie_bywidiarta;

import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private String[] dataName, durasi, tahunRilis;
    private String[] dataDescription, genre, rating;
    private TypedArray dataPhoto, dataImgAktor1, dataImgAktor2, dataImgAktor3;
    private MovieAdapter adapter;
    private ArrayList<Movie> movies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new MovieAdapter(this);
        ListView listView = findViewById(R.id.lv_list);
        listView.setAdapter(adapter);

        prepare();
        addItem();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Movie movie_sent = new Movie();
                movie_sent.setName(dataName[i]);
                movie_sent.setTahunRilis(tahunRilis[i]);
                movie_sent.setDescription(dataDescription[i]);
                movie_sent.setRating(rating[i]);
                movie_sent.setDurasi(durasi[i]);
                movie_sent.setGenre(genre[i]);
                movie_sent.setPhoto(dataPhoto.getResourceId(i,-1));
                movie_sent.setAktor1(dataImgAktor1.getResourceId(i,-1));
                movie_sent.setAktor2(dataImgAktor2.getResourceId(i, -1));
                movie_sent.setAktor3(dataImgAktor3.getResourceId(i, -1));

                Intent moveWithObjectIntent2 = new Intent(MainActivity.this, Detail_Movie.class);
                moveWithObjectIntent2.putExtra(Detail_Movie.EXTRA_TITIPAN, movie_sent);
                startActivity(moveWithObjectIntent2);
            }
        });

    }

    private void addItem(){
        movies = new ArrayList<>();

        for (int i = 0; i<dataName.length;i++){
            Movie movie = new Movie();
            movie.setPhoto(dataPhoto.getResourceId(i,-1));
            movie.setName(dataName[i]);
            movie.setDescription(dataDescription[i]);
            movies.add(movie);
        }
        adapter.setMovie(movies);
    }

    private void prepare(){
        dataName = getResources().getStringArray(R.array.data_name);
        dataDescription = getResources().getStringArray(R.array.data_description);
        tahunRilis = getResources().getStringArray(R.array.tahun_rilis);
        rating = getResources().getStringArray(R.array.rating);
        durasi = getResources().getStringArray(R.array.durasi);
        genre = getResources().getStringArray(R.array.genre);
        dataPhoto = getResources().obtainTypedArray(R.array.data_photo);
        dataImgAktor1 = getResources().obtainTypedArray(R.array.img_aktor1);
        dataImgAktor2 = getResources().obtainTypedArray(R.array.img_aktor2);
        dataImgAktor3 = getResources().obtainTypedArray(R.array.img_aktor3);
    }


}
