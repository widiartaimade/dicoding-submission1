package com.dicoding.widiarta.listmovie_bywidiarta;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewHolder {
    private TextView txtName;
    private TextView txtDescription;
    private ImageView imgPhoto;

    ViewHolder(View view) {
        txtName = view.findViewById(R.id.txt_name);
        txtDescription = view.findViewById(R.id.txt_description);
        imgPhoto = view.findViewById(R.id.img_photo);
    }

    void bind(Movie movie) {
        txtName.setText(movie.getName());
        txtDescription.setText(movie.getDescription());
        imgPhoto.setImageResource(movie.getPhoto());
    }

}
